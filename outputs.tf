output "public_ip" {

  value = module.ec2-default-vpc.public_ip
}
output "iam_policy_s3-access_key-id" {
  value = module.iam-policy-s3.iam_policy_s3-access_key-id
}

output "iam_policy_s3-access_key-secret" {
  value     = module.iam-policy-s3.iam_policy_s3-access_key-secret
  sensitive = true
}

output "public_dns" {
  value = module.ec2_nginx.public_dns
}
output "public_ip_ec2_nginx" {
  value = module.ec2_nginx.public_ip
}

output "subnet-prive-subnet-id" {
  value = module.subnet_prive.subnet-id
}

output "subnet-id_subnet2" {
  value = module.second_subnet_public.subnet-id
}

output "elb-dns_name" {
  value = module.elb.dns_name
}