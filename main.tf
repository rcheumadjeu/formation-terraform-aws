module "key-pair" {
  source                   = "./modules/key-pair"
  key_pair_public_key_file = var.public_key_file

}

module "ec2-default-vpc" {
  source = "./modules/ec2-default-vpc"
}

module "iam-policy-s3" {
  source             = "./modules/iam-policy-s3"
  iam_policy_s3-name = var.name
  iam_policy_s3-path = var.path
}

module "vpc_1_subnet_public" {
  source                 = "./modules/vpc_1_subnet_public"
  vpc-cidr               = var.vpc_cidr
  vpc-availability_zones = var.vpc_availability_zones
  vpc-name               = var.vpc_name
}

module "ec2_nginx" {
  source = "./modules/ec2_nginx"
  #ec2-instance_type     = var.instance_type
  ec2-instance_name = var.instance_name
  #ec2-instance_ami      = var.instance_ami
  ec2-instance_key_name = var.instance_key_name
  ec2-vpc-id            = module.vpc_1_subnet_public.vpc-id
  #ec2-vpc-id    = module.vpc_1_subnet_public.aws_vpc.tf.id
  ec2-subnet-id = module.vpc_1_subnet_public.subnet-id
}

module "subnet_prive" {
  source                 = "./modules/subnet_prive"
  vpc-availability_zones = var.vpc_availability_zones
  vpc-cidr               = var.vpc_cidr
  vpc-id                 = module.vpc_1_subnet_public.vpc-id
  vpc-nat_gateway-id     = module.vpc_1_subnet_public.nat_gateway-id
}

module "second_subnet_public" {
  source                 = "./modules/second_subnet_public"
  vpc-availability_zones = var.vpc_availability_zones
  vpc-cidr               = var.vpc_cidr
  vpc-id                 = module.vpc_1_subnet_public.vpc-id
  vpc-igw                = module.vpc_1_subnet_public.igw-id
  min_size               = var.min_size
  max_size               = var.max_size
  #image_id               = var.image_id
  key_name = var.instance_key_name
}

module "elb" {
  source        = "./modules/elb"
  elb-subnet-id = [module.vpc_1_subnet_public.subnet-id]
  vpc-id        = module.vpc_1_subnet_public.vpc-id
}

module "asg" {
  source       = "./modules/asg"
  asg-elb-name = module.elb.name
  asg-min_size = var.min_size
  asg-max_size = var.max_size
  #asg-image_id      = var.image_id
  asg-instance_type = var.instance_type
  asg-key_name      = var.key_name
}