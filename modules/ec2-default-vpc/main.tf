
/*resource "aws_instance" "tf" {
  ami = "ami-00068cd7555f543d5"
  instance_type = "t2.micro"
}
*/

data "aws_ami" "ubuntu" {
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["099720109477"] # Canonical
}
resource "aws_instance" "tf" {
  #iam_instance_profile   = aws_iam_instance_profile.deployment_profile.name
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = "t3.micro"
  #subnet_id              = aws_subnet.private_a.id
  #key_name               = aws_key_pair.key1.key_name
  #vpc_security_group_ids = [aws_security_group.webserver.id]
  tags = {
   # Name       = "b2-group2-webserver1"
   # Deployment = "deploy_wordpress"
  }

}