variable vpc-id {}

variable vpc-igw {}

variable vpc-availability_zones {}

variable vpc-cidr {}

variable min_size {}

variable max_size {}

#variable image_id {}

variable key_name {}