resource "aws_key_pair" "terraform" {
  key_name   = "aws_terraform"
  public_key = file(var.key_pair_public_key_file)
}